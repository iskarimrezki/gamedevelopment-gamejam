# GameDevelopment-GameJam-Bonnie & Clyde On The Run

**Game Description :**

This game demo is inspired by Bonnie Elizabeth Parker and Clyde Chestnut Barrow or known by name Bonnie & Clyde that were a Legendary American criminal couple who traveled the Central United States. After robbed a bank, they have to travel way back to their safehouse. But it's not as easy as you think because in the road you also meet some big spider that are poisonous. Try to not jump into some abyss or got bite by the spider because you only have one life!**

**Features:**
-Move Bonnie & Clyde at the same time
-Explore the map and find a way back to the safehouse.
-Some obstacles and enemy as you exploring the map

**Gameplay & Control :**
The control is quite simple,
-Up Arrow to Jump
-Left Arrow to Run to left
-Right Arrow to Run to right
