extends Area2D

export (String) var sceneName = "GameOver"

func _on_Area2D_body_entered(body):
	if body.get_name() == "Bonnie&Clyde":
        get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
