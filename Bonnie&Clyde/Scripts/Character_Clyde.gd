extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -1200

const UP = Vector2(0,-1)

var velocity = Vector2()

var is_dead = false

onready var sprite = self.get_node("Sprite")

func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('jump'):
		velocity.y = jump_speed
	if Input.is_action_pressed('right'):
		velocity.x += speed
	
	if Input.is_action_pressed('left'):
		velocity.x -= speed
	
func _physics_process(delta):
		velocity.y += delta * GRAVITY
		get_input()
		velocity = move_and_slide(velocity, UP)
	

func dead():
	is_dead = true
	velocity = Vector2(0,0)
	$AnimatedSprite.play("dead")
	$AnimatedSprite2.play("dead")
	$Timer.start()
	
func _process(delta):
	if is_dead == false :
		if velocity.y != 0:
			$AnimatedSprite.play("jump")
			$AnimatedSprite2.play("jump")
		elif velocity.x != 0:
			$AnimatedSprite.play("run")
			$AnimatedSprite2.play("run")
			if velocity.x > 0:
				$AnimatedSprite.flip_h = false
				$AnimatedSprite2.flip_h = false
			else:
				$AnimatedSprite.flip_h = true
				$AnimatedSprite2.flip_h = true
		else:
			$AnimatedSprite.play("idle")
			$AnimatedSprite2.play("idle")
		
	if get_slide_count() > 0:
		for i in range(get_slide_count()):
			if "Enemy" in get_slide_collision(i).collider.name:
				dead()
				
func _on_Area2D_body_entered(body):
	pass # Replace with function body.


func _on_Timer_timeout():
	 get_tree().change_scene(str("res://Scenes/GameOver.tscn"))