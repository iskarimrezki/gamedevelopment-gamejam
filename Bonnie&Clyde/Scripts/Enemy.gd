extends KinematicBody2D

const GRAVITY = 10
const SPEED = 100
const FLOOR = Vector2(0, -1)

var velocity = Vector2()

var direction = 1 
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func changeDirection(direction):
	return direction * -1

func _physics_process(delta):
	velocity.x = SPEED * direction
	
	if direction == 1:
		$AnimatedSprite.flip_h = true
	else :
		$AnimatedSprite.flip_h = false
	$AnimatedSprite.play("walk")
	
	velocity.y += GRAVITY
	
	velocity = move_and_slide(velocity, FLOOR)
	
	if get_slide_count()> 0 :
		for i in range (get_slide_count()):
			if "Bonnie&Clyde" in get_slide_collision(i).collider.name:
				get_slide_collision(i).collider.dead()
	
	
	if is_on_wall():
		direction = changeDirection(direction)
		#$RayCast2D.position.x *= -1
	
	if $RayCast2D.is_colliding() == false :
		direction = direction * -1
		$RayCast2D.position.x *= -1