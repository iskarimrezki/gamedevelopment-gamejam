extends Area2D

export (String) var sceneName = "Win Screen"

func _on_Area_Trigger_body_entered(body):
	var current_scene = get_tree().get_current_scene().get_name()
	if body.get_name() == "Bonnie&Clyde":
		if current_scene == sceneName:
			global.lives -=1
		if (global.lives == 0):
			get_tree().change_scene(str("res://Scenes/MainMenu.tscn"))
		else:
			get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
